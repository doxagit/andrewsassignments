﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolechoice
{
	public class Program
	{
		public static bool Choice { get; set; }
		public static string Decision { get; set; }
		public static void Main(string[] args)
		{
			Choice = false;
			while (!Choice)
			{
				Console.Write("Do you like the snow? ");

				Decision = Console.ReadLine();

				switch (Decision)
				{
					case ("yes"):
						Console.WriteLine("Not The right answer try again!");
						Console.WriteLine();
						break;
					case ("no"):
						Console.WriteLine("You got it!");
						Console.WriteLine();
						Choice = true;
						break;
					case ("maybe"):
						Console.WriteLine("Nope! try again!");
						Console.WriteLine();
						break;
				}

			}
			Console.WriteLine("Congratulations! you're a snow hater like the rest of us!");
			Console.ReadKey();
		}
	
	}
}
